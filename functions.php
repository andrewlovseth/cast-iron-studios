<?php

add_theme_support( 'title-tag' );

add_action('template_redirect', 'bearsmith_disable_author_page');

function bearsmith_disable_author_page() {
    global $wp_query;

    if ( is_author() ) {
        wp_redirect(get_option('home'), 301); 
        exit; 
    }
}

// Enqueue custom styles and scripts
function enqueue_styles_and_scripts() {
    wp_enqueue_style( 'adobe-fonts', 'https://use.typekit.net/vcx3lxt.css' );
    wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/style.css' );
}
add_action( 'wp_enqueue_scripts', 'enqueue_styles_and_scripts' );